import { expect } from "chai";
import { pure, notPure } from "../src/1-general";

describe("Test functional programming general concepts", function () {
  describe("Pure func", function () {
    it("should return 2 if input is 1", function () {
      expect(pure(1)).equal(2);
    });
  });

  describe("Not pure", function () {
    it('should return 3 if input is 2, where global x = 1', function () {
      expect(notPure(2)).equal(3);
    });
  });
});
