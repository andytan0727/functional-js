/**
 * Reactive programming is an asynchronous
 * programming paradigm concerned with data
 * streams and the propagation of change
 * 
 * RxJS (Reactive Extensions for JavaScript)
 * is a library for reactive programming using
 * observables that makes it easier to compose
 * asynchronous or callback-based code
 * 
 * for more, refer:
 * rxjs: https://rxjs.dev/guide/overview
 * angular: https://angular.io/guide/rx-library
 */

import { of } from "rxjs";
import { map, filter, pipe } from "rxjs";

const nums = of(1, 2, 3, 4, 5);
const squareOdd = pipe(
  filter((n) => n % 2 !== 0),
  map((x) => x * x),
);

const ob = squareOdd(nums);

ob.subscribe((x) => console.log(x));
