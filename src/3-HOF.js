// ignore this
console.log(`
 _   _   _____   _____  
| | | | /  _  \\ |  ___| 
| |_| | | | | | | |__   
|  _  | | | | | |  __|  
| | | | | |_| | | |     
|_| |_| \\_____/ |_|     
`);

// global result array var to ease storing results from imperative loop
// note: this is in contrast with our functional programming approach,
//       don't use this too often
let result = [];

// global input array, don't mutate this
const arr = [1, 2, 3, 4, 5];

// ==============================================================
// 1) Array.prototype.filter
// ==============================================================
// Objective: find elem > 3

// normal imperative for loop
for (let i = 0; i < arr.length; i++) {
  if (arr[i] > 3) {
    result.push(arr[i]);
  }
}
console.log(result);

// newer for-of loop
result = []; // remember to reset
for (const e of arr) {
  if (e > 3) {
    result.push(e);
  }
}
console.log(result);

// using Array.prototype.filter
console.log(arr.filter((e) => e > 3));

// ==============================================================
// 2) Array.prototype.map
// ==============================================================
// Objective: power 2 each elem

function pow2(num) {
  return Math.pow(num, 2);
}

// normal imperative for loop
result = []; // reset
for (let i = 0; i < arr.length; i++) {
  result.push(pow2(arr[i]));
}
console.log(result);

// with Array.prototype.map
console.log(arr.map(pow2));

// ==============================================================
// 3) Array.prototype.reduce
// ==============================================================
// Objective: sum all values in arr

// normal imperative for loop
let total = 0;
for (let i = 0; i < arr.length; i++) {
  total += arr[i];
}
console.log(total);

// using Array.prototype.reduce
console.log(arr.reduce((acc, val) => acc + val));

// ==============================================================
// 4) Array.prototype.flatMap
// ==============================================================
// Objective: find the average age of people
const people = [
  // group 1
  [
    {
      id: 1,
      name: "Alice",
      age: 28,
    },
    {
      id: 2,
      name: "Joseph",
      age: 30,
    },
    {
      id: 3,
      name: "Jack",
      age: 41,
    },
  ],

  // group 2
  [
    {
      id: 4,
      name: "Geralt",
      age: 55,
    },
    {
      id: 5,
      name: "Yennefer",
      age: 40,
    },
    {
      id: 6,
      name: "Ciri",
      age: 28,
    },
  ],

  // group 3
  [
    {
      id: 7,
      name: "Jonathan",
      age: 29,
    },
    {
      id: 8,
      name: "George",
      age: 38,
    },
    {
      id: 9,
      name: "Jolyne",
      age: 18,
    },
  ],
];

// normal imperative for loop
let totalAge = 0;
let totalPeople = 0;
let avgAge = 0;
for (let i = 0; i < people.length; i++) {
  totalPeople += people[i].length;

  for (let j = 0; j < people[i].length; j++) {
    totalAge += people[i][j].age;
  }
}
avgAge = totalAge / totalPeople;
console.log(Math.floor(avgAge));

// using Array.prototype.flatMap
console.log(
  people
    .flatMap((g) => g.map((p) => p.age))
    .reduce((acc, age, _, arr) => acc + age / arr.length, 0)
    .toFixed(0)
);
