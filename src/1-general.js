import cloneDeep from "lodash/cloneDeep";

// ignore this
console.log(`
 _____   _____   __   _   _____   _____        ___   _      
/  ___| | ____| |  \\ | | | ____| |  _  \\      /   | | |     
| |     | |__   |   \\| | | |__   | |_| |     / /| | | |     
| |  _  |  __|  | |\\   | |  __|  |  _  /    / / | | | |     
| |_| | | |___  | | \\  | | |___  | | \\ \\   / /  | | | |___  
\\_____/ |_____| |_|  \\_| |_____| |_|  \\_\\ /_/   |_| |_____| 
`);

// ==============================================================
// 1) Pure
// ==============================================================

/**
 * A pure function is a function which:
 *
 * - Given the same input(s), always returns the same output
 * - Has no side-effects
 *
 * @param {*} x
 */
export function pure(x) {
  return x + 1;
}

console.log(pure(1));
console.log(pure(1));
console.log(pure(1));

// example of not pure func
let x = 1; // global variable

export function notPure(y) {
  return x + y;
}

console.log(notPure(1));
x += 1;
console.log(notPure(1));
x += 1;
console.log(notPure(1));

// ==============================================================
// 2) No side-effects
// ==============================================================
let z = 1;

function side(x) {
  z += 1; // simple e.g. of side effect: change global var inside func
  return x + z;
}

console.log(side(1));
console.log(side(1));
console.log(side(1));

// ==============================================================
// 3) Immutability
// ==============================================================

// const in JS can be used to provide immutability for primitive vars
// it only disable variable reassign
const a = 1;
// a = 2;

// neither it disable changing object property
const person = {
  fname: "John",
  lname: "Doe",
  age: 28,
  spouse: {
    fname: "Karen",
    lname: "Yan",
    age: 26,
  },
};
person.fname = "James";
console.log(person);

// nor disable changing array elem
const arr = [1, 2, 3, 4, 5];
arr[0] = 0;
console.log(arr);

// Native Object.freeze provides a superficial one-level immutability
// Object.freeze(person);

person.fname = "Bob";
person.spouse.fname = "Alice";

console.log(person);

// simple immutability for object in vanilla JS
const person2 = Object.assign({}, person);
const person2_es = { ...person }; // ES6 onwards

// note that both of them doesn't deep clone object
person.spouse.fname = "Jess";

console.log(person2);
console.log(person2_es);

// deep clone using Lodash util
const cloneDeepPerson = cloneDeep(person);

person.spouse.fname = "Jasmine";
console.log(cloneDeepPerson);
