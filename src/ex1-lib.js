// ==============================================================
// 1) lodash/lodash-fp
// ==============================================================
// lodash: https://lodash.com/
// lodash-fp: https://github.com/lodash/lodash/wiki/FP-Guide

// example functions
import map from "lodash/map";
import fpMap from "lodash/fp/map";

const arr = [1, 2, 3, 4, 5];
console.log(map(arr, (x) => x * 2));

// lodash-fp functions are immutable auto-curried
const pow2Map = fpMap((x) => x * x);
console.log(pow2Map(arr));
console.log(pow2Map([10, 20, 30, 40, 50]));

// ==============================================================
// 2) Immutable.js
// ==============================================================
import { Map } from "immutable";

const map1 = Map({ a: 1, b: 2, c: 3 });
const map2 = map1.set("b", 50);
console.log(map1); // map1 remains unchanged
console.log(map2);

// equality
const map3 = Map({ a: 1, b: 2, c: 3 });
console.log(map3.equals(map1));
console.log(map3.equals(map2));

// copy, simply reference original var
const map4 = map1;
console.log(map4.set("b", 100));
console.log(map1);

// ==============================================================
// 3) Underscore js
// ==============================================================
// similar to Lodash, visit https://underscorejs.org/ for more

// ==============================================================
// 4) Lazy.js
// ==============================================================
// for more, visit: https://github.com/dtao/lazy.js
import Lazy from "lazy.js";

// find people with name starts with Nelson
const people = [
  {
    id: 1,
    name: "John",
    age: 28,
  },
  {
    id: 2,
    name: "Nelson",
    age: 33,
  },
];

const result = Lazy(people)
  .filter((p) => p.name.startsWith("Nelson"))
  .take(5);

console.log(result.value());

// 300 unique random nums between 1 & 1000
const rands = Lazy.generate(Math.random)
  .map((x) => Math.floor(x * 1000) + 1)
  .uniq()
  .take(300)
  .value();

console.log(rands);

// ==============================================================
// 5) Ramda
// ==============================================================
// more serious lib for functional programming with many "weird" functions.
// Can use this if you are all-in functional programming in your project
// Learn more: https://ramdajs.com/docs/

import * as R from "ramda";

// zip
const a = [1, 2, 3];
const b = ["a", "b", "c"];
console.log(R.zip(a, b));

// map
console.log(R.map((x) => x * x, [1,2,3]));
