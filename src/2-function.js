// ignore this
console.log(`
 _____   _   _   __   _   _____   _____   _   _____   __   _  
|  ___| | | | | |  \\ | | /  ___| |_   _| | | /  _  \\ |  \\ | | 
| |__   | | | | |   \\| | | |       | |   | | | | | | |   \\| | 
|  __|  | | | | | |\\   | | |       | |   | | | | | | | |\\   | 
| |     | |_| | | | \\  | | |___    | |   | | | |_| | | | \\  | 
|_|     \\_____/ |_|  \\_| \\_____|   |_|   |_| \\_____/ |_|  \\_| 
`);

// ==============================================================
// 1) first-class function
// ==============================================================
function addOne(num) {
  return num + 1;
}

function addAndMulTwo(num, addFunc) {
  return addFunc(num) * 2;
}

console.log('first-class func');
console.log(addAndMulTwo(2, addOne));

// ==============================================================
// 2) Closure
// ==============================================================
/**
 * A closure is a persistent local variable scope
 *
 * @param {*} num
 */
function counter() {
  let count = 1;

  function incOne() {
    return (count += 1);
  }

  return incOne;
}

const inc = counter();

console.log('counter increment');
console.log(inc());

// ==============================================================
// 3) Currying and Partial
// ==============================================================
function sum(a, b) {
  return a + b;
}

/**
 * Partial creates new function by fixing some parameters of existing one
 * 
 * @param {Function} func 
 * @param {*} param1 
 */
function partial(func, param1) {
  return function (param2) {
    return func(param1, param2);
  };
}

/**
 * Currying is a transformation of functions that translates
 * f(a, b, c) to f(a)(b)(c)
 * 
 * For more complicated example, visit
 * https://javascript.info/currying-partials
 * 
 * @param {Function} func 
 */
function curry(func) {
  return function (param1) {
    return function (param2) {
      return func(param1, param2);
    };
  };
}

// a simple way to diff partial & currying is
// Partial takes 2 or more inputs/params while
// Currying takes exactly one input/param
const partialSum = partial(sum, 4);
const currySum = curry(sum);

console.log('after applying partial & currying');
console.log(partialSum(5));
console.log(currySum(4)(5));

// with lodash
import _ from 'lodash';

const lodashPartial = _.partial(sum, 4);
const lodashCurry = _.curry(sum);

console.log('with lodash');
console.log(lodashPartial(5));
console.log(lodashCurry(4)(5));